/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description: 
 * @Date: 2024-01-11 14:55:09
 */
module.exports = function (plop) {
  plop.setGenerator('pages', {
    description: '新增一个页面',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: '请输入页面名称'
      }
    ],
    actions: [
      {
        type: 'add',
        path: 'src/pages/{{name}}/{{name}}.tsx',
        templateFile: 'template/index.jsx.hbs'
      },
      {
        type: 'add',
        path: 'src/pages/{{name}}/{{name}}.config.js',
        templateFile: 'template/index.config.js.hbs'
      },
      {
        type: 'add',
        path: 'src/pages/{{name}}/{{name}}.scss',
        templateFile: 'template/index.scss.hbs'
      }
    ] 
  });
  plop.setGenerator('component', {
    description: '新增一个自定义组件',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: '请输入组件名称'
      }
    ],
    actions: [
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.tsx',
        templateFile: 'template/index.jsx.hbs'
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.scss',
        templateFile: 'template/index.scss.hbs'
      }
    ] 
  });
}
