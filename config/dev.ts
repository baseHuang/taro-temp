/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-11 15:19:37
 */
import type { UserConfigExport } from "@tarojs/cli";

export default {
  logger: {
    quiet: false,
    stats: true,
  },
  mini: {},
  h5: {
    devServer: {
      proxy: {
        "/app-api": {
          target: "http://192.168.101.15:48086",
          changeOrigin: true,
        },
      },
    },
  },
} satisfies UserConfigExport;
