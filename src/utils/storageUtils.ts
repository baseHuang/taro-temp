/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description: storage
 * @Date: 2023-12-05 10:57:20
 */

import Taro from "@tarojs/taro";

/**
 * set
 * @param key
 * @param value
 */
export function setStorage(
  key: string,
  value: string | Record<string, string | number>
) {
  Taro.setStorageSync(key, value);
}

/**
 * get
 * @param key
 * @returns
 */
export function getStorage(key: string) {
  return Taro.getStorageSync(key);
}

/**
 * clear
 */
export function clearStorage() {
  Taro.clearStorageSync();
}

/**
 * remove
 * @param key
 */
export function removeStorage(key: string) {
  Taro.removeStorageSync(key);
}
