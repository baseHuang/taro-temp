import Taro from "@tarojs/taro";
import { isMpWeixin, isMpBaidu } from "./taroPlatform";
/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description: uni api promise化
 * @Date: 2023-12-05 10:39:18
 */

// import { nextTick } from "react";
// 路由
export function navigateTo(url: string) {
  return new Promise((resolve, reject) => {
    Taro.navigateTo({
      url: url,
      success: () => {
        resolve(true);
      },
      fail: (e: any) => {
        console.log(e);
        reject(false);
      },
    });
  });
}

// 重定向
export function redirectTo(url: string) {
  return new Promise((resolve, reject) => {
    Taro.redirectTo({
      url: url,
      success: () => {
        resolve(true);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

// 关闭所有，打开某个页面
export function reLaunch(url: string) {
  return new Promise((resolve, reject) => {
    Taro.reLaunch({
      url: url,
      success: () => {
        resolve(true);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

export function setNavigationBarTitle(title: string) {
  Taro.setNavigationBarTitle({
    title: title || "",
  });
}

// 切换tab
export function switchTab(url: string) {
  return new Promise((resolve, reject) => {
    Taro.switchTab({
      url: url,
      success: () => {
        resolve(true);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

// 返回
export function navigateBack(delta = 1) {
  return new Promise((resolve, reject) => {
    Taro.navigateBack({
      delta: delta,
      success: () => {
        resolve(true);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

// 弹框提示
export function taroToast(msg: string) {
  return new Promise((resolve) => {
    Taro.showToast({
      title: msg,
      icon: "none",
      duration: 4000,
      success: () => {
        resolve(true);
      },
    });
  });
}

// loading
export function taroLoading(title: string) {
  Taro.showLoading({ title });
}

// hide loading
export function taroHideLoading() {
  Taro.hideLoading();
}

// 模态框
export function taroModal(options: {
  title?: string;
  content?: string;
  showCancel?: boolean;
  cancelText?: string;
  cancelColor?: string;
  confirmText?: string;
  confirmColor?: string;
  editable?: boolean;
  placeholderText?: string;
  success?: (res: any) => void;
  fail?: (res: any) => void;
  complete?: () => void;
}): Promise<boolean> {
  return new Promise((resolve, reject) => {
    Taro.showModal({
      ...options,
      success: (res: { confirm: boolean; cancel: boolean }) => {
        resolve(res.confirm);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

// 复制
export function onCopy(value: string) {
  return new Promise((resolve, reject) => {
    Taro.setClipboardData({
      data: value,
      success: () => {
        taroToast("复制成功");
        resolve(true);
      },
      fail: () => {
        taroToast("复制失败，请重试");
        reject();
      },
    });
  });
}

// 获取provider
// export async function uniGetProvider() {
//   return new Promise((resolve, reject) => {
//     Taro.getProvider({
//       service: "oauth",
//       success: (res: any) => {
//         resolve(res.provider[0]);
//       },
//       fail: (err: any) => {
//         Taro.showToast({
//           title: err.errMsg,
//           icon: "none",
//         });
//         reject("");
//       },
//     });
//   });
// }

// login
export async function taroLogin(): Promise<string> {
  // const provider: any = await uniGetProvider();
  // if (provider === "") {
  //   return Promise.reject("");
  // }
  return new Promise((resolve, reject) => {
    Taro.login({
      success: (res: any) => {
        resolve(res.code);
      },
      fail: (err: any) => {
        Taro.showToast({
          title: err.errMsg,
          icon: "none",
        });
        reject("");
      },
    });
  });
}

// 检测session
export async function taroCheckSession(): Promise<boolean> {
  return new Promise((resolve, reject) => {
    Taro.checkSession({
      success() {
        resolve(true);
      },
      fail() {
        reject(false);
      },
    });
  });
}

// 拨打电话
export async function taroMakePhoneCall(phoneNumber: string) {
  return new Promise((resolve, reject) => {
    Taro.makePhoneCall({
      phoneNumber: phoneNumber,
      success: () => {
        resolve(true);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}

export async function taroPay(orderNo: string): Promise<boolean | string> {
  const params = {
    orderNo: orderNo,
    clientIp: "127.0.0.1",
    payChannel: "WX_JSAPI_PAY",
  };

  // const res: any = await etcApi.payUsingPOST(params)
  const res: any = [];
  return new Promise((resolve, reject) => {
    if (res.success) {
      if (res.amount == 0) {
        resolve("1000");
      } else {
        Taro.requestPayment({
          // provider: "wxpay",
          // orderInfo: "", // 订单数据
          timeStamp: res.payment.timeStamp,
          nonceStr: res.payment.nonceStr,
          package: "prepay_id=" + res.payment.prepayId,
          signType: res.payment.signType,
          paySign: res.payment.paySign,
          success: () => {
            resolve(res.outTradeNo);
          },
          fail: () => {
            reject(false);
          },
        });
      }
    }
  });
}

export function nextTickMock(fn: () => void) {
  // if (isMpWeixin) {
  //   Taro.nextTick(fn);
  // } else if (isMpBaidu) {
  //   swan.nextTick(fn);
  // } else {
  //   nextTick(fn);
  // }
  Taro.nextTick(fn);
}

export function taroScan(): Promise<string | boolean> {
  return new Promise((resolve, reject) => {
    Taro.scanCode({
      success: (res) => {
        resolve(res.result);
      },
      fail: () => {
        reject(false);
      },
    });
  });
}
