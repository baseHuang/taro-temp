/* eslint-disable indent */
/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 10:56:40
 */
import { isMpWeixin, isMpBaidu } from "@/utils";
import Taro, { useLoad } from "@tarojs/taro";
import { useState } from "react";

export function useCustomHeader() {
  const [statusBarHeight, setStatusBarHeight] = useState(20);
  const [contentBarHeight, setContentBarHeight] = useState<number>(40);
  console.log("isMpWeixin", isMpWeixin);
  useLoad(() => {
    if (isMpWeixin || isMpBaidu) {
      const area = Taro.getSystemInfoSync();
      const menu = Taro.getMenuButtonBoundingClientRect();
      // eslint-disable-next-line indent
      const barHeight = area.statusBarHeight
        ? menu.height + (menu.top - area.statusBarHeight!) * 2
        : isMpBaidu
        ? 60
        : 40;
      setStatusBarHeight(area.statusBarHeight!);
      setContentBarHeight(barHeight);
    }
  });

  return {
    statusBarHeight,
    contentBarHeight,
  };
}
