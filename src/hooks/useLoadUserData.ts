/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 10:52:56
 */
import { useState, useEffect } from "react";

import useGetUserInfo from "./useGetUserInfo";
import { useRequest } from "ahooks";
import { useDispatch } from "react-redux";

import { loginReducer } from "../store/reducer/userReducer";

function useLoadUserData() {
  const dispatch = useDispatch();
  const [waitingUserData, setWaitingUserData] = useState(true);

  const { username } = useGetUserInfo();

  // const { run } = useRequest(getUserInfoService, {
  //   manual: true,
  //   onSuccess(res) {
  //     // const { username, nickname } = res;
  //     // const dispatch = useDispatch()
  //     // dispatch(loginReducer({ username, nickname }));
  //     // console.log(useGetUserInfo())
  //   },
  //   onFinally() {
  //     setWaitingUserData(false);
  //   },
  // });

  useEffect(() => {
    if (username) {
      setWaitingUserData(false);
      return;
    }
    // run() //如果 redux store中没有用户信息 则请求
    // run();
  }, [username]);

  return { waitingUserData };
}
export default useLoadUserData;
