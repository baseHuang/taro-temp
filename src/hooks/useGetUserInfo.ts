/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 10:50:58
 */
import { useSelector, useDispatch } from "react-redux";
import type { StateType } from "../store";
import type { UserStateType } from "../store/reducer/userReducer";

function useGetUserInfo() {
  const { username, nickname } = useSelector<StateType>(
    (state) => state.user
  ) as UserStateType;

  return { username, nickname };
}

export default useGetUserInfo;
