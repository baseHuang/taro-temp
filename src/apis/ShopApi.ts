/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2023-12-14 14:16:09
 */
import BaseApi from "./BaseApi";

const $basePath = "/shop".replace(/\/$/, "");
export const ShopApi = {
  /**
   * @summary 获得店铺详情
   */
  shopGET<T>(params: Record<string, string | boolean | number>) {
    return BaseApi.baseGetRequest<T>($basePath + "/get", params);
  },
};
