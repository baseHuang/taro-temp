/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2023-12-05 11:16:57
 */
import { HttpRequest } from "@/http";

const BaseApi = {
  basePostRequest<T>(
    url: string,
    params: Record<string, string | number | boolean | any[]>,
    header?: Record<string, string>
  ) {
    const opts = {
      method: "post",
      url: url,
      data: params,
      header: header,
    };
    return HttpRequest<T>(opts);
  },

  basePutRequest<T>(
    url: string,
    params: Record<string, string | number | boolean>,
    header?: Record<string, string>
  ) {
    const opts = {
      method: "put",
      url: url,
      data: params,
      header: header,
    };
    return HttpRequest<T>(opts);
  },

  baseDeleteRequest<T>(
    url: string,
    params: Record<string, string | number | boolean>
  ) {
    const opts = {
      method: "delete",
      url: url,
      data: params,
    };
    return HttpRequest<T>(opts);
  },

  baseGetRequest<T>(
    url: string,
    params: Record<string, string | number | boolean>
  ) {
    const opts = {
      method: "get",
      url: url,
      data: params,
    };
    return HttpRequest<T>(opts);
  },
};

export default BaseApi;
