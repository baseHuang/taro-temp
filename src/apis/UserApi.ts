/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 10:20:26
 */
import BaseApi from "./BaseApi";

// const $basePath = "/user".replace(/\/$/, "");
const $couponPath = "/user/coupon".replace(/\/$/, "");
export const UserApi = {
  /**
   * @param params 过期记录
   * @returns
   */
  couponExpirePageGET(params: Record<string, any>) {
    return BaseApi.baseGetRequest($couponPath + "/expire/page", params);
  },
};
