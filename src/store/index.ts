/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 10:46:54
 */
import { configureStore } from "@reduxjs/toolkit";

import userReducer from "./reducer/userReducer";
import type { UserStateType } from "./reducer/userReducer";

export type StateType = {
  user: UserStateType;
};

export default configureStore({
  reducer: {
    user: userReducer,
  },
});
