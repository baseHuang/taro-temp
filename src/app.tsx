/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-11 15:19:37
 */
import { PropsWithChildren } from "react";
import { useLaunch } from "@tarojs/taro";

import { Provider } from "react-redux";
import store from "./store";

import "./app.scss";
import "uno.css";

function App({ children }: PropsWithChildren<any>) {
  useLaunch(() => {
    console.log("App launched.");
  });

  // children 是将要会渲染的页面
  return <Provider store={store}>{children}</Provider>;
}

export default App;
