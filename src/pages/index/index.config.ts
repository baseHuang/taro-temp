/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-11 15:19:37
 */
export default definePageConfig({
  navigationBarTitleText: "首页",
  navigationBarBackgroundColor: "#0066E6",
  navigationBarTextStyle: "white",
});
