/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-11 15:19:37
 */
import { View, Text } from "@tarojs/components";
import { Button, Cell } from "@nutui/nutui-react-taro";
import { useLoad } from "@tarojs/taro";
import { useEffect } from "react";

import "./index.scss";
import { ShopApi } from "@/apis";
import { useCustomHeader } from "@/hooks";

export default function Index() {
  const header = useCustomHeader();
  useLoad(() => {
    console.log("Page loaded.");
    console.log(header);
  });
  useEffect(() => {
    // ShopApi.shopGET({
    //   id: 100
    // }).then(res => {
    //   console.log(res)
    // })
  });
  return (
    <View className="index font-500 text-center">
      <Text>Hello world!</Text>
      <Button type="primary">111</Button>
      <Cell title="我是标题" extra="描述文字" />
    </View>
  );
}
