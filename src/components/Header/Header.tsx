/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-04-17 10:14:48
 */
import { View, Text } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import { FC, useEffect } from "react";

import styles from "./Header.module.scss";
// console.log(styles);
import { useCustomHeader } from "@/hooks";
import { navigateBack, navigateTo, switchTab } from "@/utils/taroApiPromising";

type PropsType = {
  title?: string;
  isBack?: boolean;
  color?: string;
  to?: string;
  subtitle?: string;
};

function onBack(to) {
  const tabUrl = [
    "/pages/index/index",
    "/pages/message/index",
    "/pages/mine/index",
  ];
  if (to) {
    if (tabUrl.includes(to)) {
      switchTab(to);
    } else {
      navigateTo(to);
    }
  } else {
    navigateBack();
  }
}

const Header: FC<PropsType> = (props: PropsType) => {
  const { contentBarHeight, statusBarHeight } = useCustomHeader();
  console.log(contentBarHeight, statusBarHeight);
  function toBack(isBack) {
    if (isBack) {
      return (
        <View className={styles.backBox} onClick={() => onBack(props.to)}>
          <View
            className={styles["header_back"]}
            style={{ color: props.color, borderColor: props.color }}
          />
        </View>
      );
    }
  }

  return (
    <View
      className={styles.headerBox}
      style={{ paddingTop: `${statusBarHeight}px` }}
    >
      <View className={styles.header}>
        {toBack(props.isBack)}
        <View
          className={styles["header_box"]}
          style={{
            height: `${contentBarHeight}px`,
            lineHeight: `${contentBarHeight}px`,
            color: props.color,
          }}
        >
          {props.title}
          {props.subtitle && (
            <Text className="color-[#FF7A22]">{props.subtitle}</Text>
          )}
        </View>
      </View>
    </View>
  );
};

export default Header;
