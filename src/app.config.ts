/*
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-11 15:19:37
 */
export default defineAppConfig({
  pages: ["pages/index/index", "pages/order/order"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "远方",
    navigationBarTextStyle: "black",
  },
  tabBar: {
    color: "#7F8389",
    selectedColor: "#54956",
    borderStyle: "black",
    backgroundColor: "#fff",
    list: [
      {
        pagePath: "pages/index/index",
        text: "首页",
        iconPath: "static/shouyeg.png",
        selectedIconPath: "static/shouye.png",
      },
      {
        pagePath: "pages/order/order",
        text: "订单",
        iconPath: "static/wodeg.png",
        selectedIconPath: "static/wode.png",
      },
    ],
  },
});
