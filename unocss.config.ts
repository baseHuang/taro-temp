import { FileSystemIconLoader } from "@iconify/utils/lib/loader/node-loaders";
import {
  defineConfig,
  presetIcons,
  transformerDirectives,
  transformerVariantGroup,
} from "unocss";
import { presetApplet, presetRemRpx, transformerApplet } from "unocss-applet";

const isApplet = process.env.TARO_ENV !== "h5" || false;

export default defineConfig({
  presets: [
    presetApplet(),
    presetRemRpx({
      mode: isApplet ? "rem2rpx" : "rpx2rem",
      baseFontSize: isApplet ? 4 : 20,
    }),
    presetIcons({
      prefix: "",
      extraProperties: {
        display: "inline-block",
        "vertical-align": "middle",
      },
      collections: {
        icon: FileSystemIconLoader("./src/assets/icons"),
      },
    }),
  ],

  theme: {
    colors: {
      "black-1": "#37373D",
      "black-2": "#54545C",
      "black-3": "#747785",
      "black-4": "#989CA8",
      "black-5": "#D6D8E5",
      "black-6": "#F5F6F9",
      "black-7": "#D7D7E7",
      "orange-1": "#ED6A23",
      "orange-2": "#FF7A22",
      "orange-3": "#FD742A",
    },
  },
  transformers: [
    transformerDirectives(),
    transformerVariantGroup(),
    transformerApplet(),
  ],
});
