<!--
 * @Author: hyq
 * @LastEditors: hyq
 * @Description:
 * @Date: 2024-01-12 11:33:54
-->

## taro 模板

1. react + redux + ahooks + react-router + sass + ts + eslint + commitlint + lint-staged + jest
2. 支持多环境配置，dev、prod
3. 支持 plop 快速创建文件
4. 支持 uncss
5. 支持 nui 库
6.
